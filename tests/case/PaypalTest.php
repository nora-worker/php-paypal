<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Paypal;

use Nora;

class PaypalTest extends \PHPUnit_Framework_TestCase
{
    public function testLoad ( )
    {
        Nora::Configure_write('paypal', [
            'app_client_id' => 'Ad3H60lPvAUoJMOWsVJ26QqlCnfxROfJWaZ6jEmlEEKSiYCQqIcJPLlioTvCm2SUAPFOCzMAeZ03ie3j',
            'app_secret'    => 'ELcFpVQAX6H4cEFcWEg09E_Tcec4J3Y6p2uiFd2_3kWPW4pVWpunDk_L-jw0-33KhkpMZRLPRahlV400',
            'app_endpoint'  => 'https://api.sandbox.paypal.com'
        ]);

        return Nora::module('paypal');
    }

    /**
     * @depends testLoad
     */
    public function testPayPalApi($m)
    {
        // アクセストークンの事前取得
        $at = $m->getAPIHandler()->AccessTokenRequest();


        // 支払いのスタート
        $py = $m->getAPIHandler()->PaymentRequest([
            "transactions" => [
                [
                    "amount" => [
                        "currency" => "USD",
                        "total" =>"12"
                    ],
                    "description" => "creating a payment"
                ]
            ],
            "payer" => [
                "payment_method" => "paypal"
            ],
            "intent" => "sale",
            "redirect_urls" => [
                "cancel_url" => "https://devtools-paypal.com/guide/pay_paypal/curl?cancel=true",
                "return_url" => "https://devtools-paypal.com/guide/pay_paypal/curl?success=true"
            ]
        ]);

        $py = $m->getAPIHandler()->PaymentRequest( );

        var_dump($py);
    }

}
