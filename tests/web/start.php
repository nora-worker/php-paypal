<?php
require_once __DIR__.'/bootstrap.php';

session_start();

Nora::module('paypal')->setHelper(
    'with9Item', function ($item_name, $price, $quantity = 1) {
        // $api = API\HAndler
        $data = [
            "transactions" => [
                [
                    "amount" => [
                        "currency" => "JPY",
                        "total" => $price,
                        "details" => [
                            "subtotal" => $price,
                        ]
                    ],
                    "item_list" => [
                        "items" => [
                            [
                                'name'     => $item_name,
                                'quantity' => $quantity,
                                'price'    => $price,
                                'currency' => "JPY"
                            ],
                        ]
                    ],
                    "description" => "$item_name"
                ]
            ],
            "payer" => [
                "payment_method" => "paypal"
            ],
            "intent" => "sale",
            "redirect_urls" => [
                "cancel_url" => "http://nora-worker.net:8080/?cancel=true",
                "return_url" => "http://nora-worker.net:8080/?success=true"
            ]
        ];

        return Nora::module('paypal')->API( )->PaymentRequest($data)->getApprovalUrl('href');
    }
);

printf("<a href='%s' target='_blank'>TEST</a>", Nora::module('paypal')->with9Item('ほげ', '500'));


$api = Nora::module('paypal')->API( );

/**
$curt = Nora::module('paypal')->Curt( );
$curt->addItem();
$api->PaymentRequest(
    $curt->toPaypalTransaction([
        'redirect_urls' => [
            '',
            ''
        ]
    ])
);
**/



if (Nora::Environment()->_GET()->has('cancel'))
{
    die('お支払いキャンセル');
}

if (Nora::Environment()->_GET()->has('success'))
{
    echo 'おっけー';
    //
    // ペイメントIDをもらう
    // paymentId=PAY-3UP530693P0399114KTW2BZA&token=EC-4PF24207LD123253F&PayerID=WMBUW4P7RXPCC
    //
    $payment_id = Nora::Environment()->_GET()->get('paymentId');
    $payer_id = Nora::Environment()->_GET()->get('PayerID');

    // 確認
    try
    {
        $res = $api->PaymentLookup($payment_id);
    }
    catch(Exception $e)
    {
        die('注文が見つかりません');
    }



    try
    {
        $res = $api->PaymentExecute(
            $payment_id,
            $payer_id
        );
    }catch(Exception $e) 
    {
        die('すでに処理された注文です');
    }


    die('お支払いサンキュー');
}





//$url = Nora::module('paypal')->API( )->PaymentRequest( )->getApprovalUrl();
//
$payment = $api->PaymentRequest([
    "transactions" => [
        [
            "amount" => [
                "currency" => "USD",
                "total" =>"110",
                "details" => [
                    "subtotal" => "100",
                    "tax" => "10"
                ]
            ],
            "item_list" => [
                "items" => [
                    [
                        'name' => 'hoge',
                        'quantity' => "50",
                        'price' => "1.00",
                        'currency' => "USD"
                    ],
                    [
                        'name' => 'hoge2',
                        'quantity' => "25",
                        'price' => "2.00",
                        'currency' => "USD"
                    ]
                ]
            ],
            "description" => "creating a payment"
        ]
    ],
    "payer" => [
        "payment_method" => "paypal"
    ],
    "intent" => "sale",
    "redirect_urls" => [
        "cancel_url" => "http://nora-worker.net:8080/?cancel=true",
        "return_url" => "http://nora-worker.net:8080/?success=true"
    ]
]);

printf('<a href="%s">支払う</a>', $payment->getApprovalUrl('href'));

// $api = API\HAndler
$payment = $api->PaymentRequest([
    "transactions" => [
        [
            "amount" => [
                "currency" => "USD",
                "total" =>"120",
                "details" => [
                    "subtotal" => "100",
                    "tax"      => "10",
                    "shipping" => "10",
                ]
            ],
            "item_list" => [
                "items" => [
                    [
                        'name' => 'hage',
                        'quantity' => "50",
                        'price' => "1.00",
                        'currency' => "USD"
                    ],
                    [
                        'name' => 'hage2',
                        'quantity' => "25",
                        'price' => "2.00",
                        'currency' => "USD"
                    ]
                ]
            ],
            "description" => "creating a payment"
        ]
    ],
    "payer" => [
        "payment_method" => "paypal"
    ],
    "intent" => "sale",
    "redirect_urls" => [
        "cancel_url" => "http://nora-worker.net:8080/?cancel=true",
        "return_url" => "http://nora-worker.net:8080/?success=true"
    ]
]);

//header('Location: '.$payment->getApprovalUrl('href'));

printf('<a href="%s">はげ支払う</a>', $payment->getApprovalUrl('href'));
