<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
require_once realpath(__DIR__.'/../..').'/vendor/autoload.php';


define('TEST_PROJECT_PATH', __DIR__);

Nora::module('paypal')->configure([
    'app_client_id' => 'Ad3H60lPvAUoJMOWsVJ26QqlCnfxROfJWaZ6jEmlEEKSiYCQqIcJPLlioTvCm2SUAPFOCzMAeZ03ie3j',
    'app_secret'    => 'ELcFpVQAX6H4cEFcWEg09E_Tcec4J3Y6p2uiFd2_3kWPW4pVWpunDk_L-jw0-33KhkpMZRLPRahlV400',
    'app_endpoint'  => 'https://api.sandbox.paypal.com'
]);
