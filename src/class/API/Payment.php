<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Paypal\API;

use Nora\Core\Module\Module;

class Payment extends Model
{
    public function getApprovalUrl($type = 'href')
    {
        foreach($this->links as $link)
        {
            if ($link['rel'] === 'approval_url')
            {
                return $link[$type];
            }
        }
        return false;
    }
}

