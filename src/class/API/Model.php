<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Paypal\API;

use Nora\Core\Module\Module;

class Model
{
    private $_data = [];

    /**
     * レスポンスからビルドする
     */
    static public function build($setting)
    {
        $setting['scope'] = explode(' ', $setting['scope']);

        $class = get_called_class();
        $token = new $class( );
        foreach($setting as $k=>$v)
        {
            $token->$k = $v;
        }
        return $token;
    }

    public function __get($key)
    {
        if (isset($this->$key))
        {
            return $this->_data[$key];
        }
        return null;
    }

    public function __set($key, $value)
    {
        $this->_data[$key] = $value;
    }

    public function __isset($key)
    {
        return isset($this->_data[$key]);
    }

    public function __debugInfo( )
    {
        return $this->_data;
    }

    public function toArray( )
    {
        return $this->_data;
    }

    static public function restore($array)
    {
        $class = get_called_class();
        $token = new $class( );
        $token->_data = $array;
        return $token;
    }
}


