<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Paypal\API;

use Nora\Core\Module\Module;
use Nora\Core\Options\OptionsAccess;

/**
 * Paypal:アプリケーション情報構造体
 */
class App
{
    use OptionsAccess;

    public function __construct( )
    {
        $this->initOptions([
            'app_client_id' => '',
            'app_secret' => '',
            'app_endpoint' => ''
        ]);
    }

    public function endppoint( )
    {
        return $this->getOption('app_endpoint');
    }

    public function credential()
    {
        return $this->getOption('app_client_id').":".$this->getOption('app_secret');
    }
}
