<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Paypal\API;

use Nora\Core\Component;
use Nora\Core\Util\Json;

/**
 * Paypal:APIハンドラー
 */
class Handler
{
    use Component\Componentable;

    private $_app;
    private $_access_token;

    public function __construct($app)
    {
        $this->setApp($app);
    }

    public function initComponentImpl( )
    {
    }

    public function setApp(App $app)
    {
        $this->_app = $app;
    }

    public function app()
    {
        return $this->_app;
    }

    public function AccessToken($token)
    {
        if(is_array($token))
        {
            $token= AccessToken::restore($token);
        }
        $this->_access_token = $token;
        return $this;
    }

    public function getAccessToken( )
    {
        if ($this->_access_token)
        {
            return $this->_access_token;
        }
        return $this->_access_token = $this->AccessTokenRequest();
    }

    public function __call($name, $args)
    {
        // コマンドオブジェクトのクラス名を生成
        $class = sprintf(__namespace__.'\\Command\\'.ucfirst($name));
        if (class_exists($class))
        {
            // コマンドを作る
            $cmd = new $class($this);
            // 引数をセット
            $cmd->setArgs($args);
            // 実行
            return $cmd->execute();
        }
        return $this->getScope()->__call($name, $args);
    }
}

