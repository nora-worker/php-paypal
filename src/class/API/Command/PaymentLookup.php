<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Paypal\API\Command;

use Nora\Module\Paypal\API\Payment;
use Nora\Core\Util\Json;

class PaymentLookup extends Base
{
    const URI='/v1/payments/payment/%s';

    protected function initCommandImpl()
    {
    }

    protected function checkResponseImpl($res)
    {
        if ($res->getInfo('http_code') === 200)
        {
            return true;
        }
        return false;
    }

    public function executeImpl( )
    {
        if (!$this->args()->has(0))
        {
            $this->handler()->err('Missing Argument For Payment Lookup');
        }
        $payment_id = $this->args()->get(0);


        $client = $this->handler()->HTTP_client([]);

        $res = $client->get(
            $this->handler()->app()->endppoint() . sprintf(self::URI, $payment_id),
            [
            ],
            [
                'Content-Type' => 'application/json',
                'Accept-Language' => 'en_US',
                'Authorization' => 'Bearer '.$this->handler()->getAccessToken()->toString()
            ]
        );

        return $res;
    }

    /**
     * 結果
     */
    public function executeSuccess ($res)
    {
        return Payment::build(Json::decode($res->getBody()));
    }
}


