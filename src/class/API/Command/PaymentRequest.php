<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Paypal\API\Command;

use Nora\Module\Paypal\API\Payment;
use Nora\Core\Util\Json;

class PaymentRequest extends Base
{
    const URI='/v1/payments/payment';

    protected function initCommandImpl()
    {
    }

    protected function checkResponseImpl($res)
    {
        if ($res->getInfo('http_code') === 201)
        {
            return true;
        }
        return false;
    }

    protected function executeImpl( )
    {
        // 引数のチェック
        if (!$this->args()->has(0))
        {
            $this->handler()->err('Missing Argument For Payment');
        }

        // HTTPクライアントの起動
        $client = $this->handler()->HTTP_client([]);


        // APIを叩く
        $res = $client->post(
            $this->handler()->app()->endppoint().self::URI,
            Json::encode(
                $this->args()->get(0)
            ),
            [
                'Content-Type' => 'application/json',
                'Accept-Language' => 'en_US',
                'Authorization' => 'Bearer '.$this->handler()->getAccessToken()->toString()
            ]
        );

        return $res;
    }

    /**
     * 結果
     */
    protected function executeSuccess ($res)
    {
        return Payment::build(Json::decode($res->getBody()));
    }
}


