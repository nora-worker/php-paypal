<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Paypal\API\Command;

use Nora\Module\Paypal\API\Payment;
use Nora\Core\Util\Json;

class PaymentExecute extends Base
{
    const URI='/v1/payments/payment/%s/execute';

    protected function initCommandImpl()
    {
    }

    protected function checkResponseImpl($res)
    {
        if ($res->getInfo('http_code') === 201)
        {
            return true;
        }
        return false;
    }

    public function executeImpl( )
    {
        if (!$this->args()->has(0) || !$this->args()->has(1))
        {
            $this->handler()->err('Missing Argument For Payment Execute');
        }
        $payment_id = $this->args()->get(0);
        $payer_id   = $this->args()->get(1);


        $client = $this->handler()->HTTP_client([]);

        $res = $client->post(
            $this->handler()->app()->endppoint() . sprintf(self::URI, $payment_id),
            Json::encode([
                'payer_id' => $payer_id
            ]),
            [
                'Content-Type' => 'application/json',
                'Accept-Language' => 'en_US',
                'Authorization' => 'Bearer '.$this->handler()->getAccessToken()->toString()
            ]
        );

        return $res;
    }

    /**
     * 結果
     */
    public function executeSuccess ($res)
    {
        return Payment::build(Json::decode($res->getBody()));
    }
}


