<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Paypal\API\Command;

use Nora\Core\Component\Component;
use Nora\Core\Util\Json;
use Nora\Core\Util\Collection\Hash;
use Nora\Module\Paypal\API\Handler;

/**
 * APIコマンドのテンプレートパターンクラス
 */
abstract class Base
{
    private $_handler;
    private $_args;

    public function __construct(Handler $handler)
    {
        $this->_handler = $handler;

        // 初期化処理
        $this->initCommand();
    }

    /**
     * 引数をセット
     */
    public function setArgs($args)
    {
        $this->_args = Hash::create(null, $args);
    }

    /**
     * 引数を取得
     */
    public function args( )
    {
        return $this->_args;
    }

    /**
     * 実行
     */
    public function execute()
    {
        $this->handler()->logDebug("Command: ".get_class($this));

        // オーバライドされた実行処理を呼び出す
        $res = $this->executeImpl();

        $this->handler()->logDebug((string) $res, 'paypal');

        // レスポンスをチェックする
        if (!$this->checkResponse($res))
        {
            // エラーがあればヘッダーをログに
            $this->handler()->logWarning((string) $res);

            // レスポンスがあればエラーメッセージが入ってるはず
            if(!empty($res->getBody()))
            {
                $data = Json::decode($res->getBody());
                $this->handler()->error(
                    (string) $res
                );
                // $data['error'].":".$data['error_description'].$res.$res->getInfo('request_header'));
            }
        }

        $this->handler()->logDebug(get_class($this).':'.$res->getInfo('request_header'));

        // エラーでなければ各コマンドの結果返却ロジックへどうぞ。
        return $this->executeSuccess($res);
    }


    protected function initCommand()
    {
        $this->initCommandImpl();
    }

    protected function handler()
    {
        return $this->_handler;
    }

    protected function checkResponse($res)
    {
        return $this->checkResponseImpl($res);
    }

    protected function checkResponseImpl($res)
    {
        if ($res->getInfo('http_code') === 200)
        {
            return true;
        }
        return false;
    }

    abstract protected function initCommandImpl();
    abstract protected function executeImpl();
    abstract protected function executeSuccess($res);
}
