<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Paypal\API\Command;

use Nora\Module\Paypal\API\AccessToken;
use Nora\Core\Util\Json;

/**
 * AccessTokenの取得
 */
class AccessTokenRequest extends Base
{
    const URI='/v1/oauth2/token';

    protected function initCommandImpl()
    {
    }

    public function executeImpl( )
    {
        $client = $this->handler()->HTTP_client([
            'auth_basic' => $this->handler()->app()->credential()
        ]);

        $res = $client->post(
            $this->handler()->app()->endppoint().'/v1/oauth2/token',
            [
                'grant_type' => 'client_credentials'
            ],
            [
                'Accept' => 'application/json',
                'Accept-Language' => 'en_US'
            ]
        );


        return $res;
    }

    /**
     * アクセストークンのリクエスト
     */
    public function executeSuccess ($res)
    {
        return AccessToken::build(Json::decode($res->getBody()));
    }
}


