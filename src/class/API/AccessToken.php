<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Paypal\API;

use Nora\Core\Module\Module;

class AccessToken extends Model
{

    public function toString()
    {
        return $this->access_token;
    }

    public function type()
    {
        return $this->type;
    }
}

