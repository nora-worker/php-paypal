<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Paypal;

use Nora\Core\Component\Component;

/**
 * Paypalのコンポーネント
 */
class Paypal extends Component
{
    private $_account;

    static public function create($scope, $config)
    {
        $paypal = new Paypal($scope);

        $paypal->setAccount($config->get('user_account'));

        return $paypal;
    }

    protected function initComponentImpl( )
    {
    }

    public function setAccount($user)
    {
        $this->_account = $user;
    }

    public function getAccount( )
    {
        return $this->_account;
    }

}
