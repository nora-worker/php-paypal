<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Paypal;

use Nora\Core\Module;

/**
 * Paypalモジュール
 */
class Facade implements Module\ModuleIF
{
    use Module\Modulable;

    protected function initModuleImpl( )
    {
    }

    public function getAPIHandler($setting = [])
    {
        $app = $this->getApp($setting);

        $handler = new API\Handler($app);
        $handler->setScope($this->newScope());
        return $handler;
    }

    public function getApp($setting = [])
    {
        $app = new API\App( );
        $app->setOption($this->configure( )->readArray('paypal') + $setting);
        return $app;
    }


    public function helloPaypal( )
    {
        return 'hello paypal';
    }
}
